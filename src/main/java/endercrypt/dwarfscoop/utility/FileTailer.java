package endercrypt.dwarfscoop.utility;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Objects;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class FileTailer implements AutoCloseable
{
	private static final Logger logger = LogManager.getLogger(FileTailer.class);
	
	private final WatchService watchService = FileSystems.getDefault().newWatchService();
	
	private final Path path;
	private final Charset charset;
	private final Callback callback;
	
	private final Thread thread;
	
	private final ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	private final RandomAccessFile access;
	
	public FileTailer(Path path, Charset charset, Callback callback) throws IOException
	{
		this.path = Objects.requireNonNull(path, "path");
		this.charset = Objects.requireNonNull(charset, "charset");
		this.callback = Objects.requireNonNull(callback, "callback");
		
		this.thread = new Thread(this::watch, "tail " + path);
		
		path.getParent().register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);
		this.access = new RandomAccessFile(path.toFile(), "r");
		long index = access.length();
		this.access.seek(index);
		logger.info("seeked to position " + index + " in " + path);
	}
	
	public void start()
	{
		logger.debug(this + " started");
		thread.start();
	}
	
	public void stop()
	{
		logger.debug(this + " stopped");
		thread.interrupt();
	}
	
	private void watch()
	{
		try
		{
			while (Thread.currentThread().isInterrupted() == false)
			{
				WatchKey key = watchService.take();
				for (WatchEvent<?> event : key.pollEvents())
				{
					if (event.kind() == StandardWatchEventKinds.ENTRY_MODIFY)
					{
						Path path = (Path) event.context();
						if (this.path.getFileName().equals(path))
						{
							progress();
						}
					}
				}
				
				if (key.reset() == false)
				{
					close(new IOException("watch service failed to watch: " + path));
				}
			}
		}
		catch (InterruptedException e)
		{
			return;
		}
	}
	
	private void progress()
	{
		try
		{
			int readBytes = 0;
			while (true)
			{
				long index = access.getFilePointer();
				long size = access.length();
				if (index > size)
				{
					index = size;
					access.seek(index);
					return;
				}
				if (index < size)
				{
					push(access.readByte());
					readBytes++;
				}
				else
				{
					break;
				}
			}
			logger.trace("Read " + readBytes + " bytes");
		}
		catch (IOException e)
		{
			close(e);
		}
	}
	
	private void push(byte b)
	{
		if (b == '\n')
		{
			byte[] bytes = buffer.toByteArray();
			String text = new String(bytes, charset);
			logger.trace("output (" + bytes.length + " bytes): " + text);
			callback.onNextLine(text);
			
			buffer.reset();
			return;
		}
		
		buffer.write(b);
	}
	
	public interface Callback
	{
		public void onNextLine(String text);
		
		public void onException(IOException e);
	}
	
	private void close(IOException cause)
	{
		logger.warn("closing " + this + " due to error", cause);
		callback.onException(cause);
		close();
	}
	
	@Override
	public void close()
	{
		logger.info(this + " closed");
		IOUtils.closeQuietly(access);
		IOUtils.closeQuietly(watchService);
		IOUtils.closeQuietly(() -> thread.interrupt());
	}
	
	@Override
	public String toString()
	{
		return "FileTailer [path=" + path + ", active=" + thread.isAlive() + "]";
	}
}
