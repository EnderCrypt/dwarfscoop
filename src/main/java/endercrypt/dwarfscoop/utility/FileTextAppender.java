package endercrypt.dwarfscoop.utility;


import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;


public class FileTextAppender
{
	private static final byte[] nothing = new byte[0];
	private static final byte[] newline = new byte[] { '\n' };
	
	private final Path path;
	private boolean initialized = false;
	
	public FileTextAppender(String path)
	{
		this(Paths.get(path));
	}
	
	public FileTextAppender(Path path)
	{
		this.path = Objects.requireNonNull(path, "path");
	}
	
	public Path getPath()
	{
		return path;
	}
	
	public void append(String line) throws FileNotFoundException, IOException
	{
		autoInitialize();
		write(line.getBytes(), newline);
	}
	
	private void autoInitialize() throws IOException, FileNotFoundException
	{
		if (initialized)
		{
			return;
		}
		initialized = true;
		
		Character lastCharacter = readLastCharacter();
		if (lastCharacter != null && lastCharacter != '\n')
		{
			write(newline);
		}
	}
	
	/**
	 * technically reads last byte, not last character
	 */
	private Character readLastCharacter() throws IOException
	{
		write(nothing);
		try (RandomAccessFile access = new RandomAccessFile(path.toFile(), "r"))
		{
			long position = access.length();
			position--;
			
			if (position < 0)
			{
				return null;
			}
			
			access.seek(position);
			return (char) access.readByte();
		}
	}
	
	private void write(byte[]... byteBytes) throws FileNotFoundException, IOException
	{
		try (BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(path.toFile(), true)))
		{
			for (byte[] bytes : byteBytes)
			{
				output.write(bytes);
			}
		}
	}
}
