package endercrypt.dwarfscoop.utility;


import java.awt.Color;

import com.googlecode.lanterna.TextColor;


public class DwarfLanterna
{
	public static TextColor.RGB toColor(Color color)
	{
		return new TextColor.RGB(color.getRed(), color.getGreen(), color.getBlue());
	}
	
	public static TextColor.RGB reverseColor(TextColor color)
	{
		return new TextColor.RGB(255 - color.getRed(), 255 - color.getGreen(), 255 - color.getBlue());
	}
}
