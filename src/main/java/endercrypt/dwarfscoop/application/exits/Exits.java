package endercrypt.dwarfscoop.application.exits;


import endercrypt.library.framework.systems.exits.ExitCode;
import endercrypt.library.framework.systems.exits.StandardExits;


public interface Exits extends StandardExits
{
	@ExitCode(code = 6, description = "Charset issue")
	public void charset();
	
	@ExitCode(code = 7, description = "Gamelog issue")
	public void gamelog();
	
	@ExitCode(code = 8, description = "Database issue")
	public void database();
	
	@ExitCode(code = 9, description = "Terminal issue")
	public void terminal();
}
