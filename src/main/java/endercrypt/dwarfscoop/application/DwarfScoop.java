package endercrypt.dwarfscoop.application;


import endercrypt.dwarfscoop.application.arguments.Arguments;
import endercrypt.dwarfscoop.application.configurations.Configurations;
import endercrypt.dwarfscoop.application.exits.Exits;
import endercrypt.dwarfscoop.application.modules.Modules;
import endercrypt.library.framework.Application;


public class DwarfScoop extends Application
{
	public static void main(String[] args)
	{
		Application.launch(DwarfScoop.class, args);
	}
	
	@Override
	public Modules modules()
	{
		return (Modules) super.modules();
	}
	
	@Override
	public Arguments arguments()
	{
		return (Arguments) super.arguments();
	}
	
	@Override
	public Configurations configurations()
	{
		return (Configurations) super.configurations();
	}
	
	@Override
	public Exits exits()
	{
		return (Exits) super.exits();
	}
}
