package endercrypt.dwarfscoop.application.configurations;


import endercrypt.dwarfscoop.application.configurations.root.database.DatabaseSettings;
import endercrypt.dwarfscoop.application.configurations.root.settings.Settings;
import endercrypt.library.commons.data.DataSource;
import endercrypt.library.framework.systems.configuration.Configuration;
import endercrypt.library.framework.systems.configuration.StandardConfigurations;


public interface Configurations extends StandardConfigurations
{
	@Configuration(location = DataSource.DISK, path = "settings.yaml")
	public Settings getSettings();
	
	@Configuration(location = DataSource.DISK, path = "database.yaml")
	public DatabaseSettings getDatabase();
}
