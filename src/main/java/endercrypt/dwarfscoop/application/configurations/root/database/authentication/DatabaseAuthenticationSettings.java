package endercrypt.dwarfscoop.application.configurations.root.database.authentication;

public class DatabaseAuthenticationSettings
{
	private String username;
	
	public String getUsername()
	{
		return username;
	}
	
	private String password;
	
	public String getPassword()
	{
		return password;
	}
}
