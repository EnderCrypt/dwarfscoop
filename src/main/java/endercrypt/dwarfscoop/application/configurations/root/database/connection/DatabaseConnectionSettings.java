package endercrypt.dwarfscoop.application.configurations.root.database.connection;

public class DatabaseConnectionSettings
{
	private String url;
	
	public String getUrl()
	{
		return url;
	}
	
	private String driver;
	
	public String getDriver()
	{
		return driver;
	}
	
	private String dialect;
	
	public String getDialect()
	{
		return dialect;
	}
}
