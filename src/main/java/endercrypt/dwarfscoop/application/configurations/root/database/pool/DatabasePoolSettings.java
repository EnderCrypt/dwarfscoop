package endercrypt.dwarfscoop.application.configurations.root.database.pool;

public class DatabasePoolSettings
{
	private int initial;
	
	public int initial()
	{
		return initial;
	}
	
	private int max;
	
	public int max()
	{
		return max;
	}
	
	private int increment;
	
	public int increment()
	{
		return increment;
	}
	
	private int timeout;
	
	public int timeout()
	{
		return timeout;
	}
	
	private boolean debug;
	
	public boolean debug()
	{
		return debug;
	}
}
