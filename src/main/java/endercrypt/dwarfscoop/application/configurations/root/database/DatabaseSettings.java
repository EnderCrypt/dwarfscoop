package endercrypt.dwarfscoop.application.configurations.root.database;

import endercrypt.dwarfscoop.application.configurations.root.database.authentication.DatabaseAuthenticationSettings;
import endercrypt.dwarfscoop.application.configurations.root.database.connection.DatabaseConnectionSettings;
import endercrypt.dwarfscoop.application.configurations.root.database.pool.DatabasePoolSettings;

public class DatabaseSettings
{
	private DatabaseConnectionSettings connection;
	
	public DatabaseConnectionSettings connection()
	{
		return connection;
	}
	
	private DatabaseAuthenticationSettings authentication;
	
	public DatabaseAuthenticationSettings authentication()
	{
		return authentication;
	}
	
	private DatabasePoolSettings pool;
	
	public DatabasePoolSettings pool()
	{
		return pool;
	}
	
	private boolean verbose;
	
	public boolean isVerbose()
	{
		return verbose;
	}
	
	private boolean statistics;
	
	public boolean isStatistics()
	{
		return statistics;
	}
}
