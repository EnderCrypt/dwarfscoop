package endercrypt.dwarfscoop.application.modules;


import endercrypt.dwarfscoop.modules.database.DatabaseModule;
import endercrypt.dwarfscoop.modules.gamelog.GameLogModule;
import endercrypt.dwarfscoop.modules.group.GroupModule;
import endercrypt.dwarfscoop.modules.organizer.OrganizerModule;
import endercrypt.dwarfscoop.modules.processor.ProcessorModule;
import endercrypt.dwarfscoop.modules.terminal.TerminalModule;
import endercrypt.dwarfscoop.modules.ticker.TickerModule;
import endercrypt.dwarfscoop.modules.ui.UiModule;
import endercrypt.library.framework.systems.modules.StandardModules;


public interface Modules extends StandardModules
{
	public DatabaseModule database();
	
	public GroupModule group();
	
	public ProcessorModule processor();
	
	public GameLogModule gameLog();
	
	public TerminalModule terminal();
	
	public UiModule ui();
	
	public OrganizerModule organizer();
	
	public TickerModule ticker();
}
