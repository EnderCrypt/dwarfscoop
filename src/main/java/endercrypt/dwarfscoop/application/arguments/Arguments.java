package endercrypt.dwarfscoop.application.arguments;


import endercrypt.library.framework.systems.argument.StandardArguments;

import java.nio.file.Path;

import org.apache.logging.log4j.core.tools.picocli.CommandLine.Option;

import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;


@Command(mixinStandardHelpOptions = true, helpCommand = true)
public class Arguments extends StandardArguments
{
	@Option(names = { "-g", "--gui" }, description = "forces a graphical interface to be used")
	private boolean gui = false;
	
	public boolean isGui()
	{
		return gui;
	}
	
	@Parameters(arity = "1")
	private Path gameLogPath;
	
	public Path getGameLogPath()
	{
		return gameLogPath;
	}
}
