package endercrypt.dwarfscoop.modules.database;


import endercrypt.dwarfscoop.application.DwarfScoop;
import endercrypt.dwarfscoop.application.configurations.root.database.DatabaseSettings;
import endercrypt.dwarfscoop.modules.database.table.BaseEntity;
import endercrypt.dwarfscoop.modules.database.transaction.DwarfScoopTransaction;
import endercrypt.library.framework.systems.modules.CoreModule;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.spi.ServiceException;
import org.reflections.Reflections;


public class DatabaseModule extends CoreModule<DwarfScoop>
{
	private static final Logger logger = LogManager.getLogger(DatabaseModule.class);
	
	private SessionFactory sessionFactory;
	
	public DatabaseModule(DwarfScoop application)
	{
		super(application);
	}
	
	@Override
	protected void onStart() throws Exception
	{
		sessionFactory = buildSessionFactory();
	}
	
	@Override
	protected void onShutdown() throws Exception
	{
		if (sessionFactory != null)
		{
			sessionFactory.close();
		}
	}
	
	public DwarfScoopTransaction open()
	{
		return new DwarfScoopTransaction(getSessionFactory().createEntityManager());
	}
	
	public SessionFactory getSessionFactory()
	{
		return sessionFactory;
	}
	
	private SessionFactory buildSessionFactory()
	{
		DatabaseSettings settings = application().configurations().getDatabase();
		
		Properties prop = new Properties();
		prop.put("jadira.usertype.autoRegisterUserTypes", "true");
		prop.put("jadira.usertype.javaZone", "UTC");
		prop.put("hibernate.jdbc.time_zone", "UTC");
		
		prop.put("hibernate.connection.CharSet", "utf8");
		prop.put("hibernate.connection.characterEncoding", "utf8");
		prop.put("hibernate.connection.passwordCharacterEncoding", "utf8");
		prop.put("hibernate.connection.useUnicode", "true");
		
		prop.put("hibernate.show_sql", settings.isVerbose());
		prop.put("connection.pool_size", settings.pool().max()); // deprecated
		prop.put("hibernate.generate_statistics", settings.isStatistics());
		
		prop.put("hibernate.c3p0.acquire_increment", settings.pool().increment());
		prop.put("hibernate.c3p0.min_size", settings.pool().increment());
		prop.put("hibernate.c3p0.max_size", settings.pool().max());
		prop.put("hibernate.c3p0.unreturnedConnectionTimeout", settings.pool().timeout());
		prop.put("hibernate.c3p0.debugUnreturnedConnectionStackTraces", settings.pool().debug());
		
		String url = settings.connection().getUrl();
		logger.info("Connecting to database on " + url);
		prop.setProperty("hibernate.connection.url", url);
		
		prop.setProperty("hibernate.dialect", settings.connection().getDialect());
		
		prop.setProperty("hibernate.connection.username", settings.authentication().getUsername());
		prop.setProperty("hibernate.connection.password", settings.authentication().getPassword());
		prop.setProperty("hibernate.connection.driver_class", settings.connection().getDriver());
		
		Configuration configuration = new Configuration();
		configuration.addProperties(prop);
		
		Reflections reflections = new Reflections(application().getInformation().getPackage() + ".modules.database.table");
		for (Class<?> entityClass : reflections.getSubTypesOf(BaseEntity.class))
		{
			logger.debug("Found model class: " + entityClass.getSimpleName());
			configuration.addAnnotatedClass(entityClass);
		}
		
		logger.debug("Setting up database session factory ...");
		try
		{
			return configuration.buildSessionFactory();
		}
		catch (ServiceException e)
		{
			logger.error("failed to setup database session factory", e);
			application().exits().database();
			return null;
		}
	}
}
