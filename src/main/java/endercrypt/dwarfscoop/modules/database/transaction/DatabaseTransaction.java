package endercrypt.dwarfscoop.modules.database.transaction;


import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;


public class DatabaseTransaction
{
	private static final Logger logger = LogManager.getLogger(DatabaseTransaction.class);
	
	protected final EntityManager entityManager;
	protected final JpaRepositoryFactory repositoryFactory;
	protected final EntityTransaction transaction;
	
	protected DatabaseTransaction(EntityManager entityManager)
	{
		this.entityManager = Objects.requireNonNull(entityManager, "entityManager");
		this.repositoryFactory = new JpaRepositoryFactory(entityManager);
		this.transaction = entityManager.getTransaction();
		this.transaction.begin();
	}
	
	public final EntityTransaction getEntityTransaction()
	{
		return transaction;
	}
	
	public final EntityManager getEntityManager()
	{
		return entityManager;
	}
	
	public void reset()
	{
		requireAlive();
		transaction.rollback();
		transaction.begin();
	}
	
	public void abort()
	{
		requireAlive();
		transaction.setRollbackOnly();
	}
	
	public void requireAlive()
	{
		if (!entityManager.isOpen())
		{
			throw new IllegalStateException("transaction is dead");
		}
	}
	
	public void finish()
	{
		requireAlive();
		if (transaction.getRollbackOnly())
		{
			logger.warn("Performed rollback on " + this);
			transaction.rollback();
		}
		else
		{
			logger.trace("Transaction " + this + " commited");
			transaction.commit();
		}
		entityManager.close();
	}
}
