package endercrypt.dwarfscoop.modules.database.transaction;


import endercrypt.dwarfscoop.modules.database.table.BaseEntity;
import endercrypt.dwarfscoop.modules.database.table.category.CategoryTable;
import endercrypt.dwarfscoop.modules.database.table.pattern.PatternTable;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;


public class DwarfScoopTransaction extends DatabaseTransaction
{
	public DwarfScoopTransaction(EntityManager entityManager)
	{
		super(entityManager);
		JpaRepositoryFactory repositoryFactory = new JpaRepositoryFactory(getEntityManager());
		
		this.group = repositoryFactory.getRepository(CategoryTable.class);
		this.pattern = repositoryFactory.getRepository(PatternTable.class);
	}
	
	public void persist(BaseEntity entity)
	{
		getEntityManager().persist(entity);
	}
	
	private final CategoryTable group;
	
	public final CategoryTable getGroup()
	{
		return group;
	}
	
	private final PatternTable pattern;
	
	public final PatternTable getPattern()
	{
		return pattern;
	}
	
}
