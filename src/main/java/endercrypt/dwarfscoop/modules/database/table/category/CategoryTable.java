package endercrypt.dwarfscoop.modules.database.table.category;


import endercrypt.dwarfscoop.modules.database.table.BaseTable;


public interface CategoryTable extends BaseTable<CategoryEntity>
{
	
}
