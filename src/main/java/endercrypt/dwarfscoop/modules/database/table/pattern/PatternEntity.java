package endercrypt.dwarfscoop.modules.database.table.pattern;


import endercrypt.dwarfscoop.modules.database.field.FieldString;
import endercrypt.dwarfscoop.modules.database.table.BaseEntity;
import endercrypt.dwarfscoop.modules.database.table.category.CategoryEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity()
@Table(name = "pattern")
public class PatternEntity extends BaseEntity
{
	// CONSTRUCTORS //
	
	@Deprecated
	public PatternEntity() // Hibernate constructor
	{
		super();
	}
	
	public PatternEntity(String pattern)
	{
		this.pattern().set(pattern);
	}
	
	// CATEGORY REFERENCE //
	
	@ManyToOne
	@JoinColumn(name = "category_id")
	private CategoryEntity category;
	
	public CategoryEntity category()
	{
		return category;
	}
	
	// PATTERN //
	
	@Column(name = "pattern", unique = true, nullable = false)
	private String pattern;
	
	@Transient
	private final FieldString fieldPattern = new FieldString(() -> pattern, (value) -> pattern = value);
	
	public FieldString pattern()
	{
		return fieldPattern;
	}
}
