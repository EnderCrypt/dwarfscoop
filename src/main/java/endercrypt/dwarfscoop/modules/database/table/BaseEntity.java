package endercrypt.dwarfscoop.modules.database.table;


import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;


@MappedSuperclass
public abstract class BaseEntity
{
	// CONSTRUCTORS //
	
	public BaseEntity() // Hibernate constructor
	{
		super();
	}
	
	// ID //
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	public Integer getId()
	{
		return id;
	}
}
