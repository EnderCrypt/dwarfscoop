package endercrypt.dwarfscoop.modules.database.table.category;


import endercrypt.dwarfscoop.modules.database.field.FieldBoolean;
import endercrypt.dwarfscoop.modules.database.field.FieldColor;
import endercrypt.dwarfscoop.modules.database.field.FieldDouble;
import endercrypt.dwarfscoop.modules.database.field.FieldString;
import endercrypt.dwarfscoop.modules.database.table.BaseEntity;
import endercrypt.dwarfscoop.modules.database.table.pattern.PatternEntity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity()
@Table(name = "category")
public class CategoryEntity extends BaseEntity
{
	// CONSTRUCTORS //
	
	@Deprecated
	public CategoryEntity() // Hibernate constructor
	{
		super();
	}
	
	public CategoryEntity(String name)
	{
		this.name().set(name);
	}
	
	// PATTERN LIST //
	
	@OneToMany(mappedBy = "category")
	@ElementCollection(targetClass = PatternEntity.class)
	private List<PatternEntity> patterns = new ArrayList<>();
	
	public List<PatternEntity> getPatterns()
	{
		return patterns;
	}
	
	// NAME //
	
	@Column(name = "name", unique = false, nullable = false, length = 128)
	private String name;
	
	@Transient
	private final FieldString fieldName = new FieldString(() -> name, (value) -> name = value);
	
	public FieldString name()
	{
		return fieldName;
	}
	
	// COLOR //
	
	@Column(name = "color", unique = false, nullable = false)
	private int color;
	
	@Transient
	private final FieldColor fieldColor = new FieldColor(() -> color, (value) -> color = value);
	
	public FieldColor color()
	{
		return fieldColor;
	}
	
	// BLINKS //
	
	@Column(name = "blinks", unique = false, nullable = false)
	private double blinks;
	
	@Transient
	private final FieldDouble fieldBlinks = new FieldDouble(() -> blinks, (value) -> blinks = value);
	
	public FieldDouble blinks()
	{
		return fieldBlinks;
	}
	
	// LIFETIME //
	
	@Column(name = "lifetime", unique = false, nullable = false)
	private double lifetime;
	
	@Transient
	private final FieldDouble fieldLifetime = new FieldDouble(() -> lifetime, (value) -> lifetime = value);
	
	public FieldDouble lifetime()
	{
		return fieldLifetime;
	}
	
	// NOTIFICATION //
	
	@Column(name = "notification", unique = false, nullable = false)
	private boolean notification;
	
	@Transient
	private final FieldBoolean fieldNotification = new FieldBoolean(() -> notification, (value) -> notification = value);
	
	public FieldBoolean notification()
	{
		return fieldNotification;
	}
}
