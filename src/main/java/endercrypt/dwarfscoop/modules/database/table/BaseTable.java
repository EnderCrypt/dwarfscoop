package endercrypt.dwarfscoop.modules.database.table;


import org.springframework.data.jpa.repository.JpaRepository;


public interface BaseTable<E extends BaseEntity> extends JpaRepository<E, Integer>
{
	
}
