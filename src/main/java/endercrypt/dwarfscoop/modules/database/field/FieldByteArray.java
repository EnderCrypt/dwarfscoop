package endercrypt.dwarfscoop.modules.database.field;


import java.util.function.Consumer;
import java.util.function.Supplier;


public class FieldByteArray extends Field<byte[]>
{
	public FieldByteArray(Supplier<byte[]> getter, Consumer<byte[]> setter)
	{
		super(getter, setter);
	}
}
