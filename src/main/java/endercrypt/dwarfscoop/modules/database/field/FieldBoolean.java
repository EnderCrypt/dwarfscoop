package endercrypt.dwarfscoop.modules.database.field;


import java.util.function.Consumer;
import java.util.function.Supplier;


public class FieldBoolean extends Field<Boolean>
{
	public FieldBoolean(Supplier<Boolean> getter, Consumer<Boolean> setter)
	{
		super(getter, setter);
	}
	
	public void setTrue()
	{
		set(true);
	}
	
	public void setFalse()
	{
		set(false);
	}
	
	public boolean isTrue()
	{
		return get() == true;
	}
	
	public boolean isFalse()
	{
		return get() == false;
	}
}
