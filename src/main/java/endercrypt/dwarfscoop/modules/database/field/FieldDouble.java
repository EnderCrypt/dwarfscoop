package endercrypt.dwarfscoop.modules.database.field;


import java.util.function.Consumer;
import java.util.function.Supplier;


public class FieldDouble extends FieldNumber<Double>
{
	public FieldDouble(Supplier<Double> getter, Consumer<Double> setter)
	{
		super(getter, setter);
	}
	
	@Override
	public void add(Double value)
	{
		set(get() + value);
	}
	
	@Override
	public void sub(Double value)
	{
		set(get() - value);
	}
	
	@Override
	public int compare(Field<Double> other)
	{
		return Double.compare(get(), other.get());
	}
}
