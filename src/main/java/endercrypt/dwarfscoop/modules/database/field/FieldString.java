package endercrypt.dwarfscoop.modules.database.field;


import java.util.function.Consumer;
import java.util.function.Supplier;


public class FieldString extends Field<String>
{
	public FieldString(Supplier<String> getter, Consumer<String> setter)
	{
		super(getter, setter);
	}
}
