package endercrypt.dwarfscoop.modules.database.field;


import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;


public class Field<T>
{
	private Supplier<T> getter;
	private Consumer<T> setter;
	
	public Field(Supplier<T> getter, Consumer<T> setter)
	{
		this.getter = getter;
		this.setter = setter;
	}
	
	public void set(T value)
	{
		setter.accept(value);
	}
	
	public void setNull()
	{
		set(null);
	}
	
	public T get()
	{
		return getter.get();
	}
	
	public boolean isNull()
	{
		return get() == null;
	}
	
	public boolean isSet()
	{
		return isNull() == false;
	}
	
	public Optional<T> getOptional()
	{
		return Optional.ofNullable(get());
	}
	
	public boolean has()
	{
		return get() != null;
	}
	
	public void update(UnaryOperator<T> operator)
	{
		set(operator.apply(get()));
	}
	
	@Override
	public String toString()
	{
		return String.valueOf(get());
	}
}
