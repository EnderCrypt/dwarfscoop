package endercrypt.dwarfscoop.modules.database.field;


import java.util.function.Consumer;
import java.util.function.Supplier;


public class FieldInteger extends FieldNumber<Integer>
{
	public FieldInteger(Supplier<Integer> getter, Consumer<Integer> setter)
	{
		super(getter, setter);
	}
	
	@Override
	public void add(Integer value)
	{
		set(get() + value);
	}
	
	@Override
	public void sub(Integer value)
	{
		set(get() - value);
	}
	
	@Override
	public int compare(Field<Integer> other)
	{
		return Integer.compare(get(), other.get());
	}
}
