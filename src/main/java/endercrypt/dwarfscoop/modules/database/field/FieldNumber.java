package endercrypt.dwarfscoop.modules.database.field;


import java.util.function.Consumer;
import java.util.function.Supplier;


public abstract class FieldNumber<T extends Number> extends Field<T>
{
	public FieldNumber(Supplier<T> getter, Consumer<T> setter)
	{
		super(getter, setter);
	}
	
	@SuppressWarnings("unchecked")
	public void increment()
	{
		add((T) (Integer) 1);
	}
	
	@SuppressWarnings("unchecked")
	public void decrement()
	{
		sub((T) (Integer) 1);
	}
	
	@SuppressWarnings("unchecked")
	public void zero()
	{
		set((T) (Integer) 0);
	}
	
	public abstract void add(T value);
	
	public abstract void sub(T value);
	
	public abstract int compare(Field<T> other);
}
