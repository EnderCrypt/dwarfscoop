package endercrypt.dwarfscoop.modules.database.field;


import endercrypt.dwarfscoop.utility.DwarfLanterna;

import java.awt.Color;
import java.util.function.Consumer;
import java.util.function.Supplier;

import com.googlecode.lanterna.TextColor;


public class FieldColor extends Field<Integer>
{
	public FieldColor(Supplier<Integer> getter, Consumer<Integer> setter)
	{
		super(getter, setter);
	}
	
	public void set(Color color)
	{
		set(color.getRGB());
	}
	
	public Color getAwtColor()
	{
		return new Color(get());
	}
	
	public TextColor.RGB getTextColor()
	{
		return DwarfLanterna.toColor(getAwtColor());
	}
}
