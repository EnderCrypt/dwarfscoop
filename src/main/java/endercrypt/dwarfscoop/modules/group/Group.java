package endercrypt.dwarfscoop.modules.group;


import endercrypt.dwarfscoop.modules.database.table.category.CategoryEntity;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.googlecode.lanterna.TextColor;


public class Group
{
	public static final String DEFAULT = "default";
	
	public static final boolean isDefault(Group group)
	{
		return group.getName().equalsIgnoreCase(Group.DEFAULT);
	}
	
	private final CategoryEntity group;
	
	private final Set<Pattern> patterns;
	
	public Group(CategoryEntity group)
	{
		this.group = group;
		
		this.patterns = group.getPatterns()
			.stream()
			.map(p -> p.pattern().get())
			.map(Pattern::compile)
			.collect(Collectors.toUnmodifiableSet());
	}
	
	public final String getName()
	{
		return group.name().get();
	}
	
	public final TextColor.RGB getColor()
	{
		return group.color().getTextColor();
	}
	
	public final double getBlinks()
	{
		return group.blinks().get();
	}
	
	public final double getLifetime()
	{
		return group.lifetime().get();
	}
	
	public final boolean isNotification()
	{
		return group.notification().get();
	}
	
	public final Set<Pattern> getPatterns()
	{
		return patterns;
	}
	
	public final boolean isMatches(String text)
	{
		for (Pattern pattern : patterns)
		{
			Matcher matcher = pattern.matcher(text);
			if (matcher.find() && matcher.hitEnd())
			{
				return true;
			}
		}
		return false;
	}
}
