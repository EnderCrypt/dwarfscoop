package endercrypt.dwarfscoop.modules.group;


import endercrypt.dwarfscoop.application.DwarfScoop;
import endercrypt.dwarfscoop.modules.database.DatabaseModule;
import endercrypt.dwarfscoop.modules.database.transaction.DwarfScoopTransaction;
import endercrypt.library.framework.systems.modules.CoreModule;
import endercrypt.library.framework.systems.modules.CoreModuleDependencies;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class GroupModule extends CoreModule<DwarfScoop>
{
	private static final Logger logger = LogManager.getLogger(GroupModule.class);
	
	private Set<Group> groups = new HashSet<>();
	private Group defaultGroup = null;
	
	public GroupModule(DwarfScoop application)
	{
		super(application);
	}
	
	@Override
	public CoreModuleDependencies dependencies()
	{
		return super.dependencies()
			.add(DatabaseModule.class);
	}
	
	@Override
	protected void onStart() throws Exception
	{
		DwarfScoopTransaction transaction = application().modules().database().open();
		try
		{
			transaction.getGroup().findAll()
				.stream()
				.map(Group::new)
				.forEach(groups::add);
		}
		catch (Exception e)
		{
			transaction.abort();
			
			logger.error("failed to read database for patterns", e);
			application().exits().database();
		}
		finally
		{
			transaction.finish();
		}
		
		for (Group group : groups)
		{
			logger.info(group.getName() + ": " + group.getPatterns());
		}
		
		defaultGroup = groups
			.stream()
			.filter(Group::isDefault)
			.findAny()
			.orElseThrow(() -> new IllegalArgumentException("no " + Group.DEFAULT + " category found"));
		
		super.onStart();
	}
	
	public Group fetch(String text)
	{
		return parse(text).orElse(defaultGroup);
	}
	
	public Optional<Group> parse(String text)
	{
		return groups
			.stream()
			.filter(g -> g.isMatches(text))
			.findAny();
	}
}
