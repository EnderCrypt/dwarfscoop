package endercrypt.dwarfscoop.modules.terminal;


import endercrypt.dwarfscoop.application.DwarfScoop;
import endercrypt.dwarfscoop.modules.ui.UiModule;
import endercrypt.library.framework.systems.information.Information;
import endercrypt.library.framework.systems.modules.CoreModule;
import endercrypt.library.framework.systems.modules.CoreModuleDependencies;

import java.awt.Dimension;
import java.io.IOException;
import java.util.Objects;

import javax.swing.JFrame;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TerminalTextUtils;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.Screen.RefreshType;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.ansi.UnixLikeTerminal.CtrlCBehaviour;
import com.googlecode.lanterna.terminal.swing.SwingTerminalFrame;


public class TerminalModule extends CoreModule<DwarfScoop>
{
	private static final Logger logger = LogManager.getLogger(TerminalModule.class);
	
	private static final DefaultTerminalFactory defaultTerminalFactory = new DefaultTerminalFactory();
	
	private Screen screen;
	
	public TerminalModule(DwarfScoop application)
	{
		super(application);
	}
	
	@Override
	public CoreModuleDependencies dependencies()
	{
		return super.dependencies()
			.add(UiModule.class);
	}
	
	@Override
	protected void onStart() throws Exception
	{
		// configure terminal factory
		Information information = application().getInformation();
		defaultTerminalFactory.setTerminalEmulatorTitle(information.getName() + " v" + information.getVersion());
		defaultTerminalFactory.setUnixTerminalCtrlCBehaviour(CtrlCBehaviour.CTRL_C_KILLS_APPLICATION);
		
		// create terminal
		Terminal terminal = createTerminal();
		logger.info("Terminal implementation: " + terminal.getClass().getSimpleName());
		
		// swing adjustments
		if (terminal instanceof SwingTerminalFrame swingTerminal)
		{
			swingTerminal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			swingTerminal.setPreferredSize(new Dimension(1000, 400));
			swingTerminal.pack();
			swingTerminal.setLocationRelativeTo(null);
			swingTerminal.setVisible(true);
			swingTerminal.setLocationRelativeTo(null);
		}
		
		// create screen
		screen = new TerminalScreen(terminal);
		screen.startScreen();
		screen.setCursorPosition(null);
		redraw();
	}
	
	private Terminal createTerminal() throws IOException
	{
		if (application().arguments().isGui())
		{
			return defaultTerminalFactory.createAWTTerminal();
		}
		return defaultTerminalFactory.createTerminal();
	}
	
	public Screen getScreen()
	{
		return screen;
	}
	
	public synchronized void redraw()
	{
		// draw
		TerminalSize newSize = screen.doResizeIfNecessary();
		if (newSize != null)
		{
			logger.info("Terminal size changed to " + newSize);
		}
		screen.clear();
		TextGraphics graphics = screen.newTextGraphics();
		application().modules().ui().draw(graphics);
		
		// refresh
		try
		{
			screen.refresh(RefreshType.COMPLETE);
		}
		catch (IOException e)
		{
			logger.error("Terminal refresh failed", e);
			application().exits().terminal();
		}
	}
	
	public static String filterString(String text)
	{
		Objects.requireNonNull(text, "text");
		StringBuilder builder = new StringBuilder();
		for (char c : text.toCharArray())
		{
			if (TerminalTextUtils.isControlCharacter(c))
			{
				c = '?';
			}
			builder.append(c);
		}
		return builder.toString();
	}
}
