package endercrypt.dwarfscoop.modules.processor;


import endercrypt.dwarfscoop.application.DwarfScoop;
import endercrypt.dwarfscoop.modules.group.Group;
import endercrypt.dwarfscoop.modules.organizer.Alert;
import endercrypt.dwarfscoop.utility.FileTextAppender;
import endercrypt.library.framework.Application;
import endercrypt.library.framework.systems.modules.CoreModule;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ProcessorModule extends CoreModule<DwarfScoop>
{
	private static final Logger logger = LogManager.getLogger(ProcessorModule.class);
	private static final FileTextAppender unknownMessageAppender = new FileTextAppender(Application.workDir.resolve("./unknown.txt"));
	
	public ProcessorModule(DwarfScoop application)
	{
		super(application);
	}
	
	public void process(String text)
	{
		Group group = application().modules().group().fetch(text);
		logger.debug("{" + group.getName() + "} " + text);
		application().modules().organizer().push(new Alert(group, text));
		
		if (Group.isDefault(group))
		{
			saveUnknownMessage(text);
		}
	}
	
	private void saveUnknownMessage(String text)
	{
		try
		{
			unknownMessageAppender.append(text);
		}
		catch (IOException e)
		{
			application().exits().ioException("failed to write text to " + unknownMessageAppender.getPath(), e);
		}
	}
}
