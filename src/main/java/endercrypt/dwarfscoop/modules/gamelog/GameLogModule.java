package endercrypt.dwarfscoop.modules.gamelog;


import endercrypt.dwarfscoop.application.DwarfScoop;
import endercrypt.dwarfscoop.modules.processor.ProcessorModule;
import endercrypt.dwarfscoop.utility.FileTailer;
import endercrypt.library.framework.Application;
import endercrypt.library.framework.systems.modules.CoreModule;
import endercrypt.library.framework.systems.modules.CoreModuleDependencies;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class GameLogModule extends CoreModule<DwarfScoop>
{
	private static final Logger logger = LogManager.getLogger(GameLogModule.class);
	
	private static final String CHARSET_NAME = "IBM437"; // https://dwarffortresswiki.org/index.php/Character_table
	
	private Path gamelogPath;
	private FileTailer tailer;
	private Charset charset;
	
	public GameLogModule(DwarfScoop application)
	{
		super(application);
	}
	
	@Override
	public CoreModuleDependencies dependencies()
	{
		return super.dependencies()
			.add(ProcessorModule.class);
	}
	
	@Override
	protected void onStart() throws Exception
	{
		try
		{
			charset = Charset.forName(CHARSET_NAME);
			logger.info("Charset: " + charset);
		}
		catch (UnsupportedCharsetException e)
		{
			logger.error("charset " + CHARSET_NAME + " missing", e);
			application().exits().charset();
			return;
		}
		
		gamelogPath = Application.workDir.resolve(application().arguments().getGameLogPath());
		logger.info("gamelog path: " + gamelogPath);
		if (Files.exists(gamelogPath) == false)
		{
			logger.error("missing gamelog file!");
			application().exits().gamelog();
			return;
		}
		if (Files.isRegularFile(gamelogPath) == false)
		{
			logger.error("gamelog is not a file!");
			application().exits().gamelog();
			return;
		}
		
		tailer = new FileTailer(gamelogPath, charset, new FileTailer.Callback()
		{
			@Override
			public void onNextLine(String text)
			{
				application().modules().processor().process(text);
			}
			
			@Override
			public void onException(IOException e)
			{
				logger.error("exception tailing " + gamelogPath, e);
				application().exits().gamelog();
			}
		});
		
		tailer.start();
		logger.info("Tailer started");
		
		super.onStart();
	}
	
	@Override
	protected void onShutdown() throws Exception
	{
		if (tailer != null)
		{
			tailer.stop();
		}
		
		super.onShutdown();
	}
	
	public Charset getCharset()
	{
		return charset;
	}
}
