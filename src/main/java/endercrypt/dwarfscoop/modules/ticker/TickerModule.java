package endercrypt.dwarfscoop.modules.ticker;


import endercrypt.dwarfscoop.application.DwarfScoop;
import endercrypt.dwarfscoop.modules.organizer.OrganizerModule;
import endercrypt.dwarfscoop.modules.terminal.TerminalModule;
import endercrypt.library.framework.systems.modules.CoreModule;
import endercrypt.library.framework.systems.modules.CoreModuleDependencies;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class TickerModule extends CoreModule<DwarfScoop>
{
	private static final Logger logger = LogManager.getLogger(TickerModule.class);
	
	private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(3);
	
	public static final double DELTA_SECONDS = 1.0 / 20;
	public static final int DELTA_MS = (int) Math.round(1000.0 * DELTA_SECONDS);
	
	private ScheduledFuture<?> future;
	
	public TickerModule(DwarfScoop application)
	{
		super(application);
	}
	
	@Override
	public CoreModuleDependencies dependencies()
	{
		return super.dependencies()
			.add(OrganizerModule.class)
			.add(TerminalModule.class);
	}
	
	@Override
	protected void onStart() throws Exception
	{
		logger.info("Ticking started at " + DELTA_SECONDS + " seconds (" + DELTA_MS + " ms)");
		future = scheduler.scheduleAtFixedRate(this::tick, DELTA_MS, DELTA_MS, TimeUnit.MILLISECONDS);
		
		super.onStart();
	}
	
	@Override
	protected void onShutdown() throws Exception
	{
		if (future != null)
		{
			future.cancel(true);
		}
		
		super.onShutdown();
	}
	
	private void tick()
	{
		try
		{
			application().modules().organizer().update();
			application().modules().terminal().redraw();
		}
		catch (Exception e)
		{
			logger.error("Ticking error", e);
		}
	}
	
	public static int timeToTicks(double time)
	{
		return (int) Math.round(time / TickerModule.DELTA_SECONDS);
	}
}
