package endercrypt.dwarfscoop.modules.ui;


import endercrypt.dwarfscoop.application.DwarfScoop;
import endercrypt.dwarfscoop.modules.organizer.Alert;
import endercrypt.dwarfscoop.modules.organizer.OrganizerModule;
import endercrypt.library.framework.systems.modules.CoreModule;
import endercrypt.library.framework.systems.modules.CoreModuleDependencies;

import java.util.Iterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.graphics.TextGraphics;


public class UiModule extends CoreModule<DwarfScoop>
{
	private static final Logger logger = LogManager.getLogger(UiModule.class);
	
	public UiModule(DwarfScoop application)
	{
		super(application);
	}
	
	@Override
	public CoreModuleDependencies dependencies()
	{
		return super.dependencies()
			.add(OrganizerModule.class);
	}
	
	public void draw(TextGraphics graphics)
	{
		TerminalSize size = graphics.getSize();
		
		OrganizerModule organizer = application().modules().organizer();
		synchronized (organizer)
		{
			Iterator<Alert> iterator = organizer.iterator();
			for (int i = 0; i < size.getRows() - 1 && iterator.hasNext(); i++)
			{
				int y = size.getRows() - i - 1;
				Alert alert = iterator.next();
				
				TerminalPosition alertPosition = new TerminalPosition(0, y);
				TerminalSize alertSize = new TerminalSize(size.getColumns(), 1);
				
				TextGraphics alertGraphics = graphics.newTextGraphics(alertPosition, alertSize);
				alert.render(alertGraphics);
			}
		}
	}
}
