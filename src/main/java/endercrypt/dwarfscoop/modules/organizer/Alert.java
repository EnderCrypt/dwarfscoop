package endercrypt.dwarfscoop.modules.organizer;


import endercrypt.dwarfscoop.modules.group.Group;
import endercrypt.dwarfscoop.modules.terminal.TerminalModule;
import endercrypt.dwarfscoop.modules.ticker.TickerModule;
import endercrypt.dwarfscoop.utility.DwarfLanterna;

import java.util.Objects;

import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;


public class Alert
{
	private static final RadixCharacters radixCharacters = new RadixCharacters(new char[] { '«', '☼', '≡', '*', '+', '-' });
	
	private final Group group;
	private final String message;
	
	private final int blinkTime;
	private final int maxAge;
	private int age = 0;
	
	public Alert(Group group, String message)
	{
		this.group = Objects.requireNonNull(group, "group");
		this.message = TerminalModule.filterString(message);
		
		this.blinkTime = TickerModule.timeToTicks(group.getBlinks());
		this.maxAge = TickerModule.timeToTicks(group.getLifetime());
	}
	
	public Group getGroup()
	{
		return group;
	}
	
	public void update()
	{
		age++;
	}
	
	public int getAge()
	{
		return age;
	}
	
	public int getMaxAge()
	{
		return maxAge;
	}
	
	public int getLifeTime()
	{
		return getMaxAge() - getAge();
	}
	
	public boolean isAlive()
	{
		return getLifeTime() > 0;
	}
	
	public double getStrength()
	{
		double strength = getLifeTime() / 35.0;
		strength = Math.pow(strength, 5.0);
		return strength;
	}
	
	public String getMessage()
	{
		return message;
	}
	
	public void render(TextGraphics graphics)
	{
		TextColor.RGB alertColor = group.getColor();
		graphics.setForegroundColor(alertColor);
		
		if (age < blinkTime && age % 4 < 2)
		{
			graphics.setBackgroundColor(DwarfLanterna.reverseColor(alertColor));
		}
		
		int level = (int) Math.floor(getStrength());
		String barString = "[" + radixCharacters.generate(level) + "]";
		graphics.putString(1, 0, barString);
		graphics.putString(barString.length() + 3, 0, message);
	}
	
	public static class Comparator implements java.util.Comparator<Alert>
	{
		@Override
		public int compare(Alert a1, Alert a2)
		{
			return Double.compare(a2.getStrength(), a1.getStrength());
		}
	}
}
