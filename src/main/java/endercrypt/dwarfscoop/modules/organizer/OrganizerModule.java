package endercrypt.dwarfscoop.modules.organizer;


import endercrypt.dwarfscoop.application.DwarfScoop;
import endercrypt.library.framework.systems.modules.CoreModule;

import java.util.Collections;
import java.util.Iterator;
import java.util.TreeSet;

import dorkbox.notify.Notify;


public class OrganizerModule extends CoreModule<DwarfScoop> implements Iterable<Alert>
{
	public static final int MAX_SIZE = 100;
	private TreeSet<Alert> alerts = new TreeSet<>(new Alert.Comparator());
	
	public OrganizerModule(DwarfScoop application)
	{
		super(application);
	}
	
	public void push(Alert alert)
	{
		if (alert.isAlive())
		{
			if (alert.getGroup().isNotification())
			{
				Notify.create()
					.title("Dwarf Fortrss")
					.text(alert.getMessage())
					.show();
			}
			synchronized (this)
			{
				alerts.add(alert);
			}
		}
	}
	
	public void update()
	{
		synchronized (this)
		{
			Iterator<Alert> iterator = alerts.iterator();
			while (iterator.hasNext())
			{
				Alert alert = iterator.next();
				alert.update();
				if (alert.isAlive() == false)
				{
					iterator.remove();
				}
			}
		}
		
		autoTrim();
	}
	
	public void autoTrim()
	{
		synchronized (this)
		{
			if (alerts.size() > MAX_SIZE)
			{
				Iterator<Alert> iterator = alerts.descendingIterator();
				while (alerts.size() > MAX_SIZE && iterator.hasNext())
				{
					iterator.next();
					iterator.remove();
				}
			}
		}
	}
	
	@Override
	public Iterator<Alert> iterator()
	{
		return Collections.unmodifiableSet(alerts).iterator();
	}
}
