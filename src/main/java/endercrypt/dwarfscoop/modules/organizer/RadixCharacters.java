package endercrypt.dwarfscoop.modules.organizer;


import java.util.Arrays;


public class RadixCharacters
{
	private char[] characters;
	
	public RadixCharacters(String characters)
	{
		this(characters.toCharArray());
	}
	
	public RadixCharacters(char[] characters)
	{
		this.characters = Arrays.copyOf(characters, characters.length);
	}
	
	public String generate(int value)
	{
		String simple = Integer.toString(value, characters.length);
		
		for (int i = 0; i < characters.length; i++)
		{
			char c = characters[characters.length - i - 1];
			simple = simple.replace(String.valueOf(i), String.valueOf(c));
		}
		
		return simple;
	}
}
